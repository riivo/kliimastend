import can # enables PiCAN communication

channel = 'can0' # CAN B on the board

sendbus = can.interface.Bus(channel=channel, bustype='socketcan_native') # bus variable for sending
listenbus = can.interface.Bus(channel=channel, bustype='socketcan_native') # bus variable for listening

listener = can.BufferedReader() # separate listen bus is needed to see messages sent by this program
notifier = can.Notifier(listenbus, [listener]) # notifier is used for buffer reading in loop()

def sendEngineTemp (temp):
    temp = int(temp) # make sure input value is an int
    if temp < 21 : temp = 21 # can't be less than 21
    elif temp > 127 : temp = 127 # can't be more than 127
    sendbus.send(can.Message(arbitration_id = 0x52c, extended_id=False,
        data = [0x23, temp*2]))

def sendAmbTemp (temp):
    temp = int(temp)
    array = (645, 642, 640, 638, 636, 634, 632, 628, 626, 624, 621, 619, 617, 610, 608, 606, 602,
            598, 592, 591, 587, 580, 576, 572, 568, 560, 557, 550, 546, 544, 538, 535, 528, 520,
            512, 511, 504, 496, 492, 489, 481, 474, 466, 464, 459, 451, 444, 436, 432, 425, 421,
            416, 410, 402, 395, 391, 384, 376, 372, 369, 365, 357, 352, 346, 339, 335, 331, 324,
            320, 316, 312, 309, 301, 298, 294, 288, 285, 282, 279, 275, 273) # no linear formula available
    d = array[temp + 30] # array indices start from 0, temperature from -30
    d1 = d // 256 # quotient to d1
    d2 = d % 256 # remainder to d2
    sendbus.send(can.Message(arbitration_id = 0x5cc, extended_id=False,
        data = [0x26, d1, d2, 0x01, 0xaa]))

def sendACSwitch(value):
    sendbus.send(can.Message(arbitration_id = 0x5f8, extended_id=False,
        data = [0x25 if value else 0x00, 0x80, 0x00, 0x00]))

def sendMessages(values): # called from loop()
    for parameter in values:
        if values[parameter][0]: # send only if active tick is checked
            if parameter == 'engineTemp':
                sendEngineTemp(values[parameter][1])
            elif parameter == 'ambTemp':
                sendAmbTemp(values[parameter][1])
            elif parameter == 'acSwitch':
                sendACSwitch(values[parameter][1])
