#!/usr/bin/python3

# MIT License

# Copyright (c) 2017

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import lexuscan # for communication with gateway ECU
import deltacan # for communication with CME-COP01
import standsmtp # for e-mail
import standgpio # for relay switching via GPIO

import threading # for putting logging into a separate thread
import time # for time.sleep in logging
import datetime # for using the current time in logfile name

from tkinter import * # tkinter is for GUI
from tkinter import ttk
from tkinter import messagebox
from tkinter import simpledialog

measurements = {} # dictionary for current, voltage, solenoid, pressure
received_ids = {} # message ids in this dictionary will be changed in the CAN messages' table, not appended
input_values = {} # holds all values from user input

padding = { 'padx': 5, 'pady': 10 } # padding between all GUI elements
logfile = "Kliimastendi logi {:%Y-%m-%d--%H-%M-%S}.csv".format(datetime.datetime.now()) # logfile name

class Frame: # sections in GUI, at the moment 4
    def __init__(self, title, name, column, row, **kw):
        self.name = name
        self.frame = ttk.Labelframe(mainframe, text = title)
        self.frame.grid(column = column, row = row, padx = 5, pady = 5, sticky = NSEW, **kw)

class Parameter: # input object in GUI
    row = 0 # to give each parameter its own row
    def __init__(self, name, frame, type, description, default, unit, **kw):
        Parameter.row += 1 # increment row
        self.name = name
        self.frame = frame
        self.value = IntVar()
        self.value.set(default) # set initial value to defined default
        self.unit = unit
        Label(frame.frame, text = description).grid(column = 2, row = self.row, **padding) # add description and place to grid
        self.label = Label(frame.frame, width = 5) # add value displaying
        self.label.grid(column = 4, row = self.row, **padding) # place to grid
        if type == 'slider': # if type is slider
            Scale(frame.frame, command = lambda x: update(self), length = 350, variable = self.value, # add a scale to grid
                    orient = HORIZONTAL, showvalue = 0, **kw).grid(column = 3, row = self.row, **padding)
            self.label.config(text = str(default) + " " + self.unit) # display default value and unit
        elif type == 'cb': # if type is checkbox
            self.cb = Checkbutton(frame.frame, command = lambda: update(self), variable = self.value, # define checkbox element
                    onvalue = "1",offvalue = "0")
            self.cb.grid(column = 3, row = self.row, **padding) # add it to grid
            self.label.config(text = 'Sees' if default else 'Väljas') # display default value
        self.active = IntVar()
        self.active.set(1)
        if frame.name == "can": # for the "can" frame, add the "active" checkbox, this allows to stop sending CAN messages
            Checkbutton(frame.frame, command = lambda: update(self), variable = self.active,
                    onvalue = "1", offvalue = "0").grid(column = 1, row = self.row, **padding)
        input_values[name] = [1, default] # append the input_values dictionary with this parameter and its default value

class Measurement:
    row = 0
    instances = [] # define list "instances" so we could loop through all measurements in loop()
    def __init__(self, name, unit, description):
        Measurement.row += 1
        self.name = name
        self.unit = unit
        Label(measurementsframe.frame, text = description).grid(column = 1, row = self.row, **padding) # description in GUI
        self.label = Label(measurementsframe.frame) # value in GUI
        self.label.grid(column = 2, row = self.row, **padding)
        measurements[name] = 0 # default value to 0, so it's displayed when no real value can be read
        Measurement.instances.append(self) # append the list instances defined above

def loop(): # main loop, initiated by root.after(0, loop) and called out again by itself until window is closed
    lexuscan.sendMessages(input_values) # send messages to gateway ECU from input_values
    measurements.update(deltacan.getMeasurements()) # get info from CME-COP01
    red_rows = [] # clear red_rows, i.e. messages with changed data
    while (lexuscan.listener.buffer.qsize()): # while there are messages in the buffer
        msg = lexuscan.listener.get_message()
        id = msg.arbitration_id
        data = "".join("%02x " % b for b in msg.data) # turn data into a string with hex values separated by " "
        if id == 0x5d5: # the message contains info about pressure and solenoid current
            measurements['pressure'] = round(msg.data[1] / 64 * 10, 3) # value in the message is in MPa*64
            measurements['solenoid'] = round(msg.data[2] / 256, 2) # value is in amperes*256
        if id not in received_ids: # if it's a message id not seen before within this session
            tree.insert('','end', id, values = (hex(id), data), tags = 'red') # add a new line, make it red
            red_rows.append(id) # append to red_rows so the line would stay red within this loop() loop
            received_ids[id] = data # append received_ids so that next time line would be updated not a new line added
        else: # id is not new
            if data == received_ids[id] and not id in red_rows: # data matched what was saved previously and
                    # id not in red_rows
                tree.item(id, values = (hex(id), data), tags = '') # update data (anyway), remove red background
            else: # id has been seen before, but data is new
                tree.item(id, values = (hex(id), data), tags = 'red') # update row, make background red
                received_ids[id] = data # update data to received_ids for comparison in next iterations
                red_rows.append(id) # append to red_rows so the line would stay red within this loop() loop
    for measure in Measurement.instances: # update the displayed measure values
        value = measurements[measure.name]
        measure.label.config(text = str(value) + " " + measure.unit)
    if measurements['current'] > 15: # if engine current is above 15 A
        deltacan.runElEngine(False) # switch off the engine
        messagebox.showwarning("Hoiatus", "Vool liiga suur!") # display a message
    root.after(500, loop) # wait 500 ms, run the same loop again

def update(parameter): # this function is called whenever the user changes an input parameter
    if parameter.name == 'coolantFan' and not parameter.value.get(): # if the user is trying to switch off coolant fan
        if not messagebox.askokcancel("Hoiatus!", # display a warning
            "Oled kindel, et soovid ventilaatori välja lülitada?"):
            parameter.cb.select() # reselect cb, if user backed out
            return # don't execute the following code, if user backed out
    if parameter.unit == 'boolean': # if checkbox
        parameter.label.config(text = 'Sees' if parameter.value.get() else 'Väljas') # no unit, just "Sees" or "Väljas"
    else:
        parameter.label.config(text = str(parameter.value.get()) + " " + parameter.unit) # if slider, display "value unit"
    input_values[parameter.name] = [parameter.active.get(), parameter.value.get()] # update input_values
    if parameter.name == 'elEngine': # if engine switched
        deltacan.runElEngine(input_values['elEngine'][1]) # send CAN message to CME
    elif parameter.frame.name == 'stand': # if GPIO switched
        standgpio.setRelays(input_values) # set relays

def appendLog(): # inserts lines into the logfile
    log.write("time, engineTemp, ambTemp, acSwitch, pressure, solenoid, elEngine, " +
            "current, voltage, acClutch, coolantFan, coolantHeating, coolantPump\r\n") # write the column headers
    while logging: # while variable logging is true
        log.write("{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}\r\n".format(
            datetime.datetime.now(), input_values['engineTemp'][1], input_values['ambTemp'][1],
            input_values['acSwitch'][1], measurements['pressure'], measurements['solenoid'],
            input_values['elEngine'][1], measurements['current'], measurements['voltage'],
            input_values['acClutch'][1], input_values['coolantFan'][1], 
            input_values['coolantHeating'][1], input_values['coolantPump'][1]))
        log.flush() # write buffer to file in each iteration, i.e. real time
        time.sleep(1) # wait 1 second
    log.close() # close the file handler after loop has finished

def onClosing(): # called when window is closed
    global logging
    logging = False # stop logging gracefully
    deltacan.runElEngine(False) # stop engine
    standgpio.disengageRelays() # open relays
    if messagebox.askyesno("Programmi sulgemine", "Kas soovid logid e-postiga edastada?"): # ask about sending e-mail
         to = simpledialog.askstring(title = "Saaja", prompt = "Sisesta e-postiaadress (mitme puhul eralda komaga): ") # if yes, give recipients
         if to: # if something given
             if standsmtp.sendLog(to, logfile): # try sending
                messagebox.showerror("Viga!", "Kirja saatmine ebaõnnestus") # sendLog returned 1, show error
    deltacan.setNode(False) # stop CME-COP01 node
    root.destroy() # destroy window

root = Tk() # main canvas
root.title("Lexus GS300 kliimastend") # window title
mainframe = ttk.Frame(root).grid() # main frame into grid, this will hold all other frames

engineecuframe = Frame("Mootori juhtmooduli simuleerimine", "can", 1, 1, columnspan = 2)
standframe = Frame("Stendi juhtimine", "stand", 1, 2)
busframe = Frame("CAN-võrk", "bus", 2, 2, rowspan = 2)
measurementsframe = Frame("Näidud", "measurements", 1, 3)

Parameter("engineTemp", engineecuframe, "slider", "Mootori temperatuur", 90, "°C", from_ = 20, to = 140)
Parameter("ambTemp", engineecuframe, "slider", "Välistemperatuur", 10, "°C", from_ = -30, to = 50)
Parameter("elEngine", standframe, "cb", "Elektrimootor", 0, "boolean")
Parameter("acClutch", standframe, "cb", "A/C magnetsidur", 0, "boolean")
Parameter("acSwitch", engineecuframe, "cb", "A/C juhtimine", 0, "boolean")
Parameter("coolantFan", standframe, "cb", "Jahutusventilaatori lülitus", 0, "boolean")
Parameter("coolantHeating", standframe, "cb", "Jahutusvedeliku kütmine", 0, "boolean")
Parameter("coolantPump", standframe, "cb", "Jahutusvedeliku pump", 0, "boolean")

Measurement("pressure", "bar", "Rõhk HI")
Measurement("solenoid", "A", "Solenoidi vool")
Measurement("current", "A", "Elektrimootori vool")
Measurement("voltage", "V", "Elektrimootiri liinipinge")

scrollbar = ttk.Scrollbar(busframe.frame) # scrollbar for the table
tree = ttk.Treeview(busframe.frame, columns = ('id', 'data'), # the table
    yscrollcommand = scrollbar.set, height = 20)
scrollbar.config(command=tree.yview) # scrollbar scrolls tree y-axis
scrollbar.grid(column= 2, row = 1, sticky = N+S, padx = (0, 10), pady = 10)
tree['show'] = 'headings'
tree.heading('id', text = 'ID')
tree.heading('data', text = 'Andmeväli')
tree.column('id', width = 70, stretch=NO)
tree.grid(column = 1, row = 1, ipadx = 5, padx = (10, 0), pady = 10)
tree.tag_configure('red', background='red') # define tag 'red'

log = open("logid/" + logfile, "a+") # create logfile
logging = True # enable logging
threading.Thread(target=appendLog).start() # start logging thread
deltacan.setNode(True) # start CME-COP01 node
root.after(0, loop) # start loop() loop
root.protocol("WM_DELETE_WINDOW", onClosing) # define function called when closing
root.mainloop() # start application
