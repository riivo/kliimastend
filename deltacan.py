import can # library for PiCAN communication
import time # needed for time.sleep

channel = 'can1' # CAN B on the board
module_id = 0x3d # needs to match the wheel settings on CME-COP01

bus = can.interface.Bus(channel=channel, bustype='socketcan_native') # bus variable. enabled bus.send method

listener = can.BufferedReader() # needed to read messages from CME
notifier = can.Notifier(bus, [listener]) # allows to access messages in buffer

def setNode(start): # starts-stops the node. in case stopped -- run led blinks; started -- run led constant green
    bus.send(can.Message(arbitration_id = 0, extended_id = False,
        data = [ 0x01 if start else 0x02, module_id ])) # 1st byte 1 to start, 2 to stop. 2nd byte is node id

def runElEngine(run): # starts stops the engine
    msg = can.Message(arbitration_id = module_id + 0x200, extended_id=False, # id is 0x23d
        data = [0x02 if run else 0x01, 0x00]) # 1st byte 2 to run, 1 to stop. 2nd byte is 0x00
    bus.send(msg)

def requestCurrent():
    msg = can.Message(arbitration_id = module_id + 0x600, extended_id=False, # id 0x63d
        data = [0x40, 0x21, 0x29, 0x05, 0x00, 0x00, 0x00, 0x00]) # 4th byte 0x05 for current
    bus.send(msg)

def requstVoltage():
    msg = can.Message(arbitration_id = module_id + 0x600, extended_id=False,
        data = [0x40, 0x21, 0x29, 0x0a, 0x00, 0x00, 0x00, 0x00]) # 4th byte 0x0a for voltage
    bus.send(msg)

def getMeasurements():
    requestCurrent()
    time.sleep(0.1) # without waiting, CME won't always reply to both
    requstVoltage()
    data = {'current': 0, 'voltage': 0} # set to 0 in case of no answer from CME
    while (listener.buffer.qsize()): # while there are messages in the listen buffer
        msg = listener.get_message()
        if msg.arbitration_id == module_id + 0x580: # reply id is module_id + 0x580
            if msg.data[3] == 0x05: # if 4th byte is 0x05, the data shows current
                data['current'] = msg.data[4] / 10 # data is in deciampers, needs to be divided by 10
            elif msg.data[3] == 0x0a: # if 4th byte is 0x05, the data shows voltage
                data['voltage'] = (msg.data[5] * 256 + msg.data[4]) / 10 # decivolts, 6th byte (data[5]) is
                    # increased after 5th byte reaches every 25,5 V
    return data # return a dictionary with 'current' and 'voltage'
