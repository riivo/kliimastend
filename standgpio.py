import RPi.GPIO as GPIO # library needed for GPIO manipulation

GPIO.setwarnings(False) # supress warnings in console
GPIO.setmode(GPIO.BOARD) # define pins by board numbering
GPIO.setup(32, GPIO.OUT) # coolant pump
GPIO.setup(36, GPIO.OUT) # coolant heating
GPIO.setup(38, GPIO.OUT) # coolant fan
GPIO.setup(40, GPIO.OUT) # ac clutch

def setACClutch(enabled):
    GPIO.output(40, True if enabled else False)

def setCoolantFan(enabled):
    GPIO.output(38, True if enabled else False)

def setCoolantHeating(enabled):
    GPIO.output(36, True if enabled else False)

def setCoolantPump(enabled):
    GPIO.output(32, True if enabled else False)

def setRelays(values): # called when user changes checkbox values
    for parameter in values:
        if parameter == 'acClutch':
            setACClutch(values[parameter][1])
        elif parameter == 'coolantFan':
            setCoolantFan(values[parameter][1])
        elif parameter == 'coolantHeating':
            setCoolantHeating(values[parameter][1])
        elif parameter == 'coolantPump':
            setCoolantPump(values[parameter][1])

def disengageRelays(): # called when closing the application
    setACClutch(False)
    setCoolantFan(False)
    setCoolantHeating(False)
    setCoolantPump(False)
