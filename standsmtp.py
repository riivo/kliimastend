import smtplib # library for using SMTP
import email.mime.multipart # libraries for composing e-mail messages
import email.mime.text
import email.mime.application

def sendLog(to, filename): # called from onClosing()
    try:
        server = smtplib.SMTP('smtp.gmail.com', 587) # define server
        server.ehlo()
        server.starttls()
        server.login("kliimastend@tktk.ee", "***") # password is needed

        msg = email.mime.multipart.MIMEMultipart() # define message variable
        msg['subject'] = "Logifail" # subject
        msg['from'] = "TTK kliimastend" # from
        msg['to'] = to # recipients
        msg.attach(email.mime.text.MIMEText("See on stendilt saadetud kiri. " +
            "Logifail manuses.")) # content
        file = open("logid/" + filename, "rb")
        attachment = email.mime.application.MIMEApplication(file.read(),
                Name = filename) # create attachment
        attachment['Content-Disposition'] = 'attachment; filename="' + filename + '"'
        msg.attach(attachment) # append attachment

        server.sendmail("kliimastend@tktk.ee", to, msg.as_string()) # send
        server.close() # close connection
    except:
        return 1 # return 1 in case something failed
